# Ubuntu Party 19.10 - Notes

## Supprimer le launcher Amazon :

    sudo apt purge ubuntu-web-launchers

## Cas d'une installation qui bloque au moment du choix de la langue ou des éléments à installer :

- Il s'agit souvent d'un disque dur défectueux (blocage de l'install au moment du scan des disques)
- Penser à activer SMART dans le Bios si nécessaires pour faire les tests.

## os-prober partiel :

Le but est d'empêcher `os-prober` de scanner certaines partitions, qui ne seront donc pas inscrites dans le Grub :

- Fichier `/etc/default/grub` :

Ajouter la ligne `GRUB_OS_PROBER_SKIP_LIST="UUID@/dev/sdX","UUID@/dev/sdY"`

- Supprimer le menu Memtest de Grub :

Il faut retirer les droits d'exécution sur le fichier qui gère le menu Memtest :

    sudo chmod -x /etc/grub.d/20_memtest86+
    
- [os-prober partiel - Ubuntu-fr](https://doc.ubuntu-fr.org/tutoriel/grub2_parametrage_manuel#os-prober_partiel)
    
## Activer toutes les mises à jour automatiques :

- Fichier `/etc/apt/apt.conf.d/50unattended-upgrades` :

Décommenter les lignes et passer à `"true"` les valeurs suivantes :

    "${distro_id}:${distro_codename}-updates";
    "${distro_id}:${distro_codename}-proposed";
    Unattended-Upgrade::Remove-Unused-Kernel-Packages "true";
    Unattended-Upgrade::Remove-New-Unused-Dependencies "true";
    Unattended-Upgrade::Remove-Unused-Dependencies "true";

## Outils :

- [FAI - Fully Automated Installation](https://fai-project.org/)
- [Bodhi Linux, distribution ultra-légère](https://www.bodhilinux.com/)
- [Transifex, plateforme de traduction pour outils libres](https://fr.transifex.com/)
- [Collabora](https://www.collaboraoffice.com/)
- [Open Project](https://www.openproject.org/fr/)
- [X2Go, bureau distant](https://wiki.x2go.org/doku.php)
- [WAPT, gestion de parcs informatiques Windows](https://www.tranquil.it/gerer-parc-informatique/decouvrir-wapt/)
- [Les Fourmis du web, dépôt WAPT](https://wapt.lesfourmisduweb.org/)
- [Salt](https://fr.wikipedia.org/wiki/Salt_(logiciel))
- [LemonLDAP-ng](https://lemonldap-ng.org/welcome)/

## Autres :

- Sur Osmand, attention à baisser au maximum la limite de stockage de données pour éviter trop de mise en cache (à creuser).
- Attention à donner un DNS différent à l'intranet et au site public d'une structure.